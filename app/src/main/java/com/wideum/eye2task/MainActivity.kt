package com.wideum.eye2task

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.ImageButton

import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener
import com.github.barteksc.pdfviewer.util.FitPolicy

import com.shockwave.pdfium.PdfDocument
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), OnPageChangeListener, OnLoadCompleteListener, OnPageErrorListener {

    internal var pdfView by Delegates.notNull<PDFView>()
    internal var pdfBackPage: ImageButton? = null
    internal var pdfNextPage: ImageButton? = null
    internal var defaultPageNumber = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pdfView = findViewById(R.id.pdfView)
        pdfNextPage = findViewById(R.id.pdfNextPage)
        pdfBackPage = findViewById(R.id.pdfBackPage)
        displayFromAsset()

        val decorView = window.decorView
        val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        decorView.systemUiVisibility = uiOptions

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {

        if (event.action == KeyEvent.ACTION_UP) {
            when (event.keyCode) {
                KeyEvent.KEYCODE_DPAD_LEFT -> moverioBackToMethod()
                KeyEvent.KEYCODE_DPAD_RIGHT -> moverioJumpToMethod()
                else -> {
                }
            }
        }
        return super.dispatchKeyEvent(event)
    }

    fun arrowBackPageVisible() {

        if (pdfView.currentPage == 0) {

            pdfBackPage?.visibility = View.GONE

        } else {

            pdfBackPage?.visibility = View.VISIBLE
        }
    }

    fun arrowNextPageVisible() {

        if (pdfView.currentPage + 1 == pdfView.pageCount) {

            pdfNextPage?.visibility = View.GONE

        } else {

            pdfNextPage?.visibility = View.VISIBLE
        }
    }

    fun moverioJumpToMethod() {
        if (pdfView.currentPage != pdfView.pageCount) pdfView.jumpTo(pdfView.currentPage + 1)
    }

    fun moverioBackToMethod() {
        if (pdfView.currentPage >= 1) pdfView.jumpTo(pdfView.currentPage - 1)
    }

    fun jumpToMethod(v: View) {
        if (pdfView.currentPage != pdfView.pageCount) pdfView.jumpTo(pdfView.currentPage + 1)
    }

    fun backToMethod(v: View) {
        if (pdfView.currentPage >= 1) pdfView.jumpTo(pdfView.currentPage - 1)
    }

    private fun displayFromAsset() {

        pdfView.fromAsset(SAMPLE_FILE)
                .defaultPage(defaultPageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                .onLoad(this) // called after document is loaded and starts to be rendered
                .onPageError(this)
                .pageFitPolicy(FitPolicy.BOTH)
                .pageFling(true) //make a fling change only a single page like ViewPager
                .pageSnap(true)
                .autoSpacing(true)
                .enableAntialiasing(true)
                .enableSwipe(false)
                .enableDoubletap(false)
                .load()
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        arrowBackPageVisible()
        arrowNextPageVisible()
    }

    fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.title, b.pageIdx))

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    override fun onPageError(page: Int, t: Throwable) {
        Log.e(TAG, "Cannot load page $page")
    }

    override fun loadComplete(nbPages: Int) {

        val meta = pdfView.documentMeta
        Log.e(TAG, "title = " + meta!!.title)
        Log.e(TAG, "author = " + meta.author)
        Log.e(TAG, "subject = " + meta.subject)
        Log.e(TAG, "keywords = " + meta.keywords)
        Log.e(TAG, "creator = " + meta.creator)
        Log.e(TAG, "producer = " + meta.producer)
        Log.e(TAG, "creationDate = " + meta.creationDate)
        Log.e(TAG, "modDate = " + meta.modDate)

        printBookmarksTree(pdfView.tableOfContents, "-")
    }

    companion object {

        private val TAG = MainActivity::class.java!!.getSimpleName()
        val PERMISSION_CODE = 42042
        val SAMPLE_FILE = "sample.pdf"
    }
}
